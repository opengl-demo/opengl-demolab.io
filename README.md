# opengl-demo.gitlab.io

![Debian popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=libgl1+libglx0+libglvnd0+libvulkan1+libegl1+libgles2+libopengl0+libgles1&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=2016-01-01&to_date=&hlght_date=&date_fmt=%25Y)

# Official documentation
* [Reference Guides](https://khronos.org/developers/reference-cards/)
* [Wiki](https://khronos.org/opengl/wiki)
  * [Related toolkits and APIs](https://khronos.org/opengl/wiki/Related_toolkits_and_APIs)

# Unofficial documentation
* [opengl-tutorial](http://www.opengl-tutorial.org/)
* OpenGL: [*Display a smooth shaded triangle with OpenGL*
  ](https://rosettacode.org/wiki/OpenGL)
  (2021) Rosetta Code
* Haskell Wiki: [*GLFW*
  ](https://wiki.haskell.org/GLFW)

# More libraries for getting OpenGL context
* [nanogui](https://github.com/mitsuba-renderer/nanogui)

# Documentation by parts
## EGL
* [egl tutorial](https://google.com/search?q=egl+tutorial)
* [*EGL guide for beginners [closed]*
  ](https://stackoverflow.com/questions/19212145/egl-guide-for-beginners)
  (2017) StackOverflow

## OpenGL ES
### Books
* [*OpenGL ES 3.0 Programming Guide*](https://worldcat.org/search?q=OpenGL+ES+3.0+Programming+Guide)
  2014 (Second edition) Ginsburg, Dan (Addison-Wesley)
* [OpenGL ES 3.0 Cookbook](https://worldcat.org/search?q=OpenGL+ES+3.0+Cookbook)
* WebGL ancestor.

## WebGL
* [*OpenGL ES for the Web*](https://khronos.org/webgl)
* [khronos.org/webgl/wiki](https://khronos.org/webgl/wiki/)
* [Getting Started](https://khronos.org/webgl/wiki/Getting_Started)
* [WebGL Fundamentals](https://webglfundamentals.org/)
* [*WebGL*](https://en.wikipedia.org/wiki/WebGL) WikipediA
* [*List of WebGL frameworks*
  ](https://en.m.wikipedia.org/wiki/List_of_WebGL_frameworks)
* [*Canvas element*
  ](https://en.wikipedia.org/wiki/Canvas_element) WikipediA
* OpenGL: [*JavaScript (WebGL)*
  ](https://rosettacode.org/wiki/OpenGL#JavaScript_.28WebGL.29)
  (2021) Rosetta Code
* WebGPU Ancestor and derivative of OpenGL ES.

### libraries
![Debian Popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=libopenscenegraph161+libjs-three&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=2017-01-01&to_date=&hlght_date=&date_fmt=%25Y)

#### Three.js (by far most popular)
* [threejs.org](https://threejs.org)
* [*three.js*
  ](https://www.npmjs.com/package/three)
  (npm)
  * The current builds only include a WebGL renderer but WebGPU (experimental), SVG and CSS3D renderers are also available as addons.
* [*Three.js*
  ](https://en.wikipedia.org/wiki/Three.js)
  (WikipediA)
  * High-level libraries such as Three.js or GLGE, Scene.js, PhiloGL,...
* [*Three.js*
    ](https://www.tutorialspoint.com/threejs)
  (Tutorials Point)
* https://repology.org/project/js:three

#### By alphabetical order...
#### Babylon.js
* [www.babylonjs.com](https://www.babylonjs.com)
  * [specifications](https://www.babylonjs.com/specifications)
    * Transparent WebGL 1.0 / WebGL 2.0 / WebGPU support
* [*babylonjs*
  ](https://www.npmjs.com/package/babylonjs)
  (npm)

#### D3
* [d3js.org](https://d3js.org)
* [*d3*
  ](https://www.npmjs.com/package/d3)
  (npm)

#### GLGE
* https://github.com/supereggbert/GLGE (Last modified in 2014)
* [*GLGE (programming library)*
  ](https://en.wikipedia.org/wiki/GLGE_(programming_library))
  (WikipediA)

#### philogl
* [*philogl*
  ](https://www.npmjs.com/package/philogl)
  (npm)

#### Scene.js
* [*Scene.js*
  ](https://www.npmjs.com/package/scenejs)
  (npm)

#### The X Toolkit: WebGL™ for Scientific Visualization (XTK)
* https://github.com/xtk/X

#### Others
* A-Frame (VR), PlayCanvas, OSG.JS, Google’s model-viewer and CopperLicht. (from WikipediA)

### Books
* *Learn Three.js*
  2023-02 (Fourth Edition) Jos Dirksen (Packt)
* *Going the Distance with Babylon.js*
  2022 Josh Elster
  * Write WebGL/WebGPU shader code using the Node Material Editor
* *Build Your Own 2D Game Engine and Create Great Web Games Using HTML5, JavaScript, and WebGL2*
 2022 (2nd ed.) Pavleas, Jebediah (Apress)
* *AR and VR Using the WebXR API Learn to Create Immersive Content with WebGL, Three.js, and A-Frame*
  2021 Baruah, Rakesh (Apress)
* *Real-Time 3D Graphics with WebGL 2*
  2018 (2nd ed.) Farhad Ghayour and Diego Cantor (Packt)
* *Physics for JavaScript Games, Animation, and Simulations With HTML5 Canvas*
  2014 Dobre, Adrian (Apress)
* *WebGL programming guide : interactive 3D graphics programming with WebGL*
  2013 Matsuda, Kouchi (Addison-Wesley)
* *HTML5 canvas*
  2013 Fulton, Steve (O'Reilly)
* *Learning HTML5 game programming : a hands-on guide to building online games using Canvas, SVG, and WebGL*
  2012 James Lamar Williams (Addison-Wesley)

## WebGPU
* [*WebGPU W3C Working Draft*
  ](https://w3.org/TR/webgpu)
* [*WebGPU*
  ](https://en.wikipedia.org/wiki/WebGPU) (WikipediA)
* WebGL derivative

# Comments
* Wayland can use EGL
* WebGL has similarities with OpenGL ES

# Languages bindings
## Python
* [*PyOpenGL Documentation*](http://pyopengl.sourceforge.net/documentation)

# OpenGL before version 3.3
* NeHe examples (see opengl-tutorial first)
